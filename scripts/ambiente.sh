#!/bin/bash

NOME_USUARIO_SISTEMA="$1"
SENHA_USUARIO_SISTEMA="$2"
NOME_USUARIO_REDE="$3"
SENHA_USUARIO_REDE="$4"
DOMINIO_REDE="$5"
ATIVO_PROXY="$6"
ENDERECO_PROXY="$7"
PORTA_PROXY="$8"

#-----------------------------------------------------------------------
# Proxy & Repositorio --------------------------------------------------
#-----------------------------------------------------------------------

if [ "$ATIVO_PROXY" -gt "0" ]; then

  # Configura proxy temporário
  grep Proxy /etc/apt/apt.conf > /dev/null 2>&1
  if [ "$?" -gt "0" ]; then
    PROXY=$NOME_USUARIO_REDE:$SENHA_USUARIO_REDE@$ENDERECO_PROXY:$PORTA_PROXY
    echo "Acquire::http::Proxy \"http://$PROXY\";" >> /etc/apt/apt.conf
    echo "Acquire::https::Proxy \"https://$PROXY\";" >> /etc/apt/apt.conf
    echo "Acquire::ftp::proxy \"ftp://$PROXY\";" >> /etc/apt/apt.conf
    echo "Acquire::socks::proxy \"socks://$PROXY\";" >> /etc/apt/apt.conf
  fi
  
  # Atualiza repositório uma vez a cada dia
  #ULTIMA_ATUAL="$(($(date +'%s') - $(stat -c %Y '/var/cache/apt')))"
  #if [ "$ULTIMA_ATUAL" -gt "86400" ]; then
    apt-get update
    apt-get -y upgrade
  #fi
  
  # Instala proxy local
  if [ ! -f /usr/sbin/cntlm ]; then
    apt-get -y install cntlm
    mv /etc/cntlm.conf /etc/cntlm.conf.off
    echo -e "Username\t$NOME_USUARIO_REDE" >> /etc/cntlm.conf
    echo -e "Domain\t\t$DOMINIO_REDE" >> /etc/cntlm.conf
    echo -e "Password\t$SENHA_USUARIO_REDE" >> /etc/cntlm.conf
    echo -e "Proxy\t\t$ENDERECO_PROXY:$PORTA_PROXY" >> /etc/cntlm.conf
    echo -e "NoProxy\t\tlocalhost, 127.0.0.*, 10.*, 192.168.*, .tse.jus.br" >> /etc/cntlm.conf
    echo -e "Listen\t\t3128" >> /etc/cntlm.conf
    echo -e "Allow\t\t127.0.0.1" >> /etc/cntlm.conf
    chmod 600 /etc/cntlm.conf
    service cntlm restart
  fi
  

  # Configura proxy permanente
  grep Proxy /etc/apt/apt.conf > /dev/null 2>&1
  if [ "$?" -eq "0" ]; then
    echo 'Acquire::http::Proxy "http://127.0.0.1:3128";' > /etc/apt/apt.conf
    echo 'Acquire::https::Proxy "https://127.0.0.1:3128";' >> /etc/apt/apt.conf
    echo 'Acquire::ftp::proxy "ftp://127.0.0.1:3128/";' >> /etc/apt/apt.conf
    echo 'Acquire::socks::proxy "socks://127.0.0.1:3128/";' >> /etc/apt/apt.conf
  fi
  grep http_proxy /etc/environment > /dev/null 2>&1
  if [ "$?" -gt "0" ]; then
    echo 'http_proxy=http://127.0.0.1:3128' >> /etc/environment
    echo 'https_proxy=https://127.0.0.1:3128' >> /etc/environment
    echo 'ftp_proxy=ftp://127.0.0.1:3128' >> /etc/environment
  fi
  export http_proxy=http://127.0.0.1:3128
  export https_proxy=https://127.0.0.1:3128
  export ftp_proxy=ftp://127.0.0.1:3128

fi



#-------------------------------------------------------------------
# Repositorio ------------------------------------------------------
#-------------------------------------------------------------------

# Configura repositório para mirror no Brasil
if [ ! -f /etc/apt/sources.list.off ]; then
  mv /etc/apt/sources.list /etc/apt/sources.list.off
  cp /vagrant/modificadores/apt/sources.list /etc/apt
fi

apt-get update
apt-get -y upgrade

# Instala ferramentas para auxiliar a automação
[ ! -f /usr/bin/expect ] && apt-get install -y expect 

# Configura atualização sem interatividade
export DEBIAN_FRONTEND=noninteractive



#-----------------------------------------------------------------------
# Interface Gráfica ----------------------------------------------------
#-----------------------------------------------------------------------

# Instala Lubuntu
if [ ! -d /usr/lib/xorg ];then
  apt-get install -y xorg
  apt-get install -y --no-install-recommends lubuntu-core
  apt-get install -y language-pack-pt
  apt-get install -y `check-language-support -l pt`
  #apt-get install -y `check-language-support -l en`
fi

# Instala adições para cliente VirtualBox
if [ ! -f /usr/bin/VBoxClient ]; then
  apt-get install -y virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11
  
  # Configura integração da vm com host
  VBoxClient --clipboard
  VBoxClient --draganddrop
  VBoxClient --display
  VBoxClient --checkhostversion
  VBoxClient --seamless
fi

# Instala ferramentas complementares da interface gráfica
[ ! -f /usr/bin/lxterminal ] && apt-get install -y libvte9 lxterminal
[ ! -f /usr/bin/lxshortcut ] && apt-get install -y lxshortcut
[ ! -f /usr/bin/terminator ] && apt-get install -y terminator


#-----------------------------------------------------------------------
# Usuário padrão -------------------------------------------------------
#-----------------------------------------------------------------------

# Adiciona usuário com senha
id $NOME_USUARIO_SISTEMA > /dev/null 2>&1
if [ "$?" -gt "0" ]; then
  useradd -m -G sudo $NOME_USUARIO_SISTEMA
  usermod --password $(echo $SENHA_USUARIO_SISTEMA | openssl passwd -1 -stdin) $NOME_USUARIO_SISTEMA

  # Configura login automático do usuário 'pje'
  echo "[Seat:*]" > /etc/lightdm/lightdm.conf
  echo "pam-service=lightdm" >> /etc/lightdm/lightdm.conf
  echo "pam-autologin-service=lightdm-autologin" >> /etc/lightdm/lightdm.conf
  echo "autologin-user=pje" >> /etc/lightdm/lightdm.conf
  echo "autologin-user-timeout=0" >> /etc/lightdm/lightdm.conf
  
  # Configura diretório ~/bin
  sudo -u $NOME_USUARIO_SISTEMA mkdir /home/$NOME_USUARIO_SISTEMA/bin
  echo '#!/bin/bash' > /etc/profile.d/00-bin-usuario.sh
  echo 'PATH="${PATH}:~/bin:~/.local/bin"' >> /etc/profile.d/00-bin-usuario.sh
fi



#-------------------------------------------------------------------
# Navegadores ------------------------------------------------------
#-------------------------------------------------------------------

# Instala Firefox
[ ! -f /usr/bin/firefox ] && apt-get install -y firefox

# Instala Chrome
if [ ! -f /usr/bin/google-chrome ]; then
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 
  sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
  apt-get update
  apt-get install -y google-chrome-stable
fi



#-------------------------------------------------------------------
# Oracle Java 8 ----------------------------------------------------
#-------------------------------------------------------------------

# Executa procedimento automatizado
# Adiciona repositorio
#add-apt-repository -y ppa:webupd8team/java
#apt update
# Instala sem interatividade
#echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
#echo debconf shared/accepted-oracle-license-v1-1 seen true |  debconf-set-selections
#apt install -y oracle-java8-installer

# Executa procedimento manual
UJDK='162'
if [ ! -d "/usr/lib/jvm/jdk1.8.0_$UJDK" ]; then
  [ ! -d /usr/lib/jvm ] && mkdir /usr/lib/jvm
  cd /usr/lib/jvm
  tar -xvzf /vagrant/instaladores/jdk-8u$UJDK-linux-x64.tar.gz
  echo '#!/bin/bash' > /etc/profile.d/01-java.sh
  echo "PATH=\"\${PATH}:/usr/lib/jvm/jdk1.8.0_$UJDK/bin:/usr/lib/jvm/jdk1.8.0_$UJDK/db/bin:/usr/lib/jvm/jdk1.8.0_$UJDK/jre/bin\"" >> /etc/profile.d/01-java.sh
  echo "J2SDKDIR=\"/usr/lib/jvm/jdk1.8.0_$UJDK\"" >> /etc/environment
  echo "J2REDIR=\"/usr/lib/jvm/jdk1.8.0_$UJDK/jre\"" >> /etc/environment
  echo "JAVA_HOME=\"/usr/lib/jvm/jdk1.8.0_$UJDK\"" >> /etc/environment
  echo "DERBY_HOME=\"/usr/lib/jvm/jdk1.8.0_$UJDK/db\"" >> /etc/environment
  update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_$UJDK/bin/java" 0
  update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_$UJDK/bin/javac" 0
  update-alternatives --set java /usr/lib/jvm/jdk1.8.0_$UJDK/bin/java
  update-alternatives --set javac /usr/lib/jvm/jdk1.8.0_$UJDK/bin/javac
  cd -
fi



#-------------------------------------------------------------------
# Utilitários para desenvolvimento ---------------------------------
#-------------------------------------------------------------------

# Instala Maven
if [ ! -d /opt/apache-maven ]; then
  tar -xvzf /vagrant/instaladores/apache-maven-3.5.2-bin.tar.gz -C /opt
  ln -sfr "/opt/$(ls /opt | grep apache-maven- )" /opt/apache-maven
  sudo -u $NOME_USUARIO_SISTEMA ln -sf /opt/apache-maven/bin/mvn /home/pje/bin/mvn
fi

# Configura Maven
if [ ! -d /home/$NOME_USUARIO_SISTEMA/.m2 ]; then
  sudo -u $NOME_USUARIO_SISTEMA mkdir /home/$NOME_USUARIO_SISTEMA/.m2
  sudo -u $NOME_USUARIO_SISTEMA unzip /vagrant/modificadores/maven/m2_repo.zip -d /home/$NOME_USUARIO_SISTEMA/.m2
  sudo -u $NOME_USUARIO_SISTEMA cp /vagrant/modificadores/maven/settings.xml /home/$NOME_USUARIO_SISTEMA/.m2
fi

# Instala Ant
if [ ! -d /opt/apache-ant ]; then
  tar -xvzf /vagrant/instaladores/apache-ant-1.10.1-bin.tar.gz -C /opt
  ln -sfr "/opt/$(ls /opt | grep apache-ant- )" /opt/apache-ant
  sudo -u $NOME_USUARIO_SISTEMA ln -sf /opt/apache-ant/bin/ant /home/pje/bin/ant
fi

# Instala ferramentas de versionamento
[ ! -f /usr/bin/git ] && apt-get install -y git
[ ! -f /usr/bin/git-cola ] && apt-get install -y git-cola
[ ! -d /usr/share/git-flow ] && apt-get install -y git-flow

# Instala ferramentas de edição / comparação
[ ! -f /usr/bin/meld ] && apt-get install -y meld
[ ! -f /usr/bin/geany ] && apt-get install -y  geany

# Instala utilitários de compressão
[ ! -f /usr/bin/zip ] && apt-get install -y zip
[ ! -f /usr/bin/unzip ] && apt-get install -y unzip
[ ! -f /bin/bzip2 ] && apt-get install -y bzip2



exit 0

