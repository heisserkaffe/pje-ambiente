#-------------------------------------------------------------------
# JBoss EAP --------------------------------------------------------
#-------------------------------------------------------------------

echo "Instalando JBoss EAP 7..."
unzip /vagrant/instaladores/jboss-eap-7.0.0.zip -d /srv
ln -sfr /srv/jboss-eap-7.0 /srv/jboss
cp -r /vagrant/modificadores/jboss/* /srv/jboss
chown -R pje. /srv/jboss-eap-7.0
