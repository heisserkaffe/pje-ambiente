#!/bin/bash

NOME_USUARIO_GIT="$1"
SENHA_USUARIO_GIT="$2"
URLS="$3"
NOME_USUARIO_SISTEMA="$4"
DIRETORIO="/home/$NOME_USUARIO_SISTEMA/git"

[ ! -d "$DIRETORIO" ] && sudo -u "$NOME_USUARIO_SISTEMA" mkdir "$DIRETORIO"
cd "$DIRETORIO"
sudo -u "$NOME_USUARIO_SISTEMA" /vagrant/scripts/clonar_repositorio_git.exp "$NOME_USUARIO_GIT" "$SENHA_USUARIO_GIT" $URLS
cd -



exit 0
