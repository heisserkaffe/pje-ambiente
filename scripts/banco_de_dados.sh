#-------------------------------------------------------------------
# PostgreSQL -------------------------------------------------------
#-------------------------------------------------------------------

BACKUP_BASE_PJE='pje_seteq'
BACKUP_BASE_PJE_BIN='pje_bin_seteq'

# Instala servidor PostgreSQL
echo "Instalando o servidor PostgreSQL..."
service postgresql status > /dev/null
[ "$?" -gt "0" ] && apt-get install -y postgresql

# Instala ferramenta PGAdmin3
echo "Instalando ferramenta PgAdmin3..."
[ ! -f /usr/bin/pgadmin3 ] && apt-get install pgadmin3

# Configura/restaura banco de dados
echo "Criando roles necessárias para os usuários da base de dados..."
sudo -u postgres psql --username=postgres --no-password -f "/vagrant/modificadores/postgresql/cria_roles.sql"
sudo -u postgres psql -lqt | cut -d'|' -f1 | grep -w pje > /dev/null
if [ "$?" -gt "0" ]; then
  echo "Criando a base de dados 'pje'..."
  sudo -u postgres createdb -E LATIN1 --lc-collate=C --lc-ctype=C -O postgres -T template0 pje
  echo "Restaurando backup da base de dados 'pje'..."
  sudo -u postgres pg_restore --dbname "pje" --no-password --section pre-data --section data --section post-data "/vagrant/modificadores/postgresql/$BACKUP_BASE_PJE"
fi
sudo -u postgres psql -lqt | cut -d'|' -f1 | grep -w pje_bin > /dev/null
if [ "$?" -gt "0" ]; then
  echo "Criando a base de dados 'pje-bin'..."
  sudo -u postgres createdb -E LATIN1 --lc-collate=C --lc-ctype=C -O postgres -T template0 pje_bin
  echo "Restaurando backup da base de dados 'pje-bin'..."
  sudo -u postgres pg_restore --dbname "pje_bin" --no-password --section pre-data --section data --section post-data "/vagrant/modificadores/postgresql/$BACKUP_BASE_PJE_BIN"
fi
CONF=$(find /etc/postgresql -name postgresql.conf)
grep "listen_addresses = 'localhost'" $CONF > /dev/null
if [ "$?" -eq "0" ]; then
  echo "Configurando arquivo de configuração 'postgresql.conf'..."
  sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '\*'/g" $CONF
  sed -i "s/#password_encryption = on/password_encryption = on/g" $CONF
  sed -i "s/#max_prepared_transactions = 0/max_prepared_transactions = 10/g" $CONF
fi
exit 0
HBA=$(find /etc/postgresql -name pg_hba.conf)
grep "0.0.0.0/0" $HBA > /dev/null
if [ "$?" -gt "0" ]; then
  echo "Configurando arquivo de configuração 'pg_hba.conf'..."
  echo -e "host\tall\t\tall\t\t0.0.0.0/0\t\tmd5" >> $HBA
  sed -i -r "0,/peer$/{s/peer$/ident/}" $HBA
  sed -i -r "s/peer$/md5/g" $HBA
fi

# Reinicia banco de dados
echo "Reiniciando banco de dados..."
service postgresql restart
    
# Ajusta a senha de usuarios do banco
echo "Redefinindo senha de usuários do banco..."
/vagrant/scripts/alterar_senha_usuario_postgresql.exp postgres 12345678
/vagrant/scripts/alterar_senha_usuario_postgresql.exp pje pje

