#!/bin/bash

NOME_USUARIO_SISTEMA="$1"


if [ ! -d "/opt/eclipse" ]; then
  unzip /vagrant/instaladores/eclipse-jee.zip -d /opt
  chown -R $NOME_USUARIO_SISTEMA. /opt/eclipse
  sudo cp /vagrant/modificadores/eclipse.desktop /usr/share/applications
  sudo -u $NOME_USUARIO_SISTEMA mkdir -p /home/$NOME_USUARIO_SISTEMA/.config/lxpanel/LXDE/panels/
  sudo -u $NOME_USUARIO_SISTEMA cp /vagrant/modificadores/panel /home/$NOME_USUARIO_SISTEMA/.config/lxpanel/LXDE/panels/
fi

